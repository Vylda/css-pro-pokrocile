# CSS pro pokročilé

## Nevidíte data z vašeho kurzu?
- Najděte rozbalovací nabídku kousek nahoře vlevo nad seznamem souborů, ve které vidíte slovo „main“ (vedle ní je text „css-pro-pokrocile“).
- Klikněte na ni.
- V sekci Tags klikněte na datum Vašeho kurzu.
